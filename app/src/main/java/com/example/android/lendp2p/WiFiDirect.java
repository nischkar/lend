package com.example.android.lendp2p;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.android.lendp2p.db.DBAdapter;
import com.example.android.lendp2p.model.DeviceDTO;
import com.example.android.lendp2p.notification.NotificationToast;
import com.example.android.lendp2p.transfer.DataHandler;
import com.example.android.lendp2p.transfer.DataSender;
import com.example.android.lendp2p.transfer.TransferConstants;
import com.example.android.lendp2p.utils.ConnectionUtils;
import com.example.android.lendp2p.utils.DialogUtils;
import com.example.android.lendp2p.utils.Utility;
import com.example.android.lendp2p.wifidirect.WiFiDirectBroadcastReceiver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class WiFiDirect extends AppCompatActivity implements PeerListFragment.OnListFragmentInteractionListener
		, WifiP2pManager.ConnectionInfoListener, WifiP2pManager.PeerListListener , WifiP2pManager.GroupInfoListener{

	/*
	TODO(" Fix the unicast message, cannot be received by the other device. BUT THE Broadcast works? :P ")
	TODO(" ALLOW THE CHAT ACTIVITY TO BE STARTED WITHOUT THE REQUEST ")
	TODO(" TRY IF POSSIBLE, MAKE THE chatlistener static? 6969 or another port that can be used ")
	TODO(" MAKE THE MULTI HOP GROUP ")
	TODO(" CONNECT GATEWAY DEVICE TO WIFI as legacy")
	TODO(" UPDATE the connected device about the devices in the same group to allow communication ")
	TODO(" GROUP OWNER HOPPING")
	 */
	public static final String FIRST_DEVICE_CONNECTED = "first_device_connected";
	public static final String KEY_FIRST_DEVICE_IP = "first_device_ip";

	private static final String SERVICE_INSTANCE = "Lend";
	private static final String SERVICE_TYPE = "_lend._tcp";

	private static final String TAG = ".WD ";
	private static final String WRITE_PERMISSION = Manifest.permission.WRITE_EXTERNAL_STORAGE;
	private static final int WRITE_PERM_REQ_CODE = 19;

	PeerListFragment deviceListFragment;
	View progressBar;

	Context cxt;
	Tusta tusta;
	DeviceDTO deviceDTO=new DeviceDTO();
	WifiP2pManager wifiP2pManager;
	WifiP2pManager.Channel wifip2pChannel;
	WiFiDirectBroadcastReceiver wiFiDirectBroadcastReceiver;
	Map<String, String>  record= new HashMap<>();
	boolean isConnectionInfoSent = false;
	private boolean isWifiP2pEnabled = false;
	private boolean isWDConnected = false;
	private boolean isGroupOwnerPresent = false;
	private boolean isHandlerPosted = false;
	private AppController appController;

	private int timeout = 10000;

	private String ssid= "null";
	private String passphrase="null";
	private BroadcastReceiver localDashReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			switch (Objects.requireNonNull(intent.getAction())) {

				case FIRST_DEVICE_CONNECTED:
					appController.startConnectionListener(ConnectionUtils.getPort(WiFiDirect.this));

					String senderIP = intent.getStringExtra(KEY_FIRST_DEVICE_IP);
					int port = DBAdapter.getInstance(WiFiDirect.this).getDevice
							(senderIP).getPort();
					DataSender.sendCurrentDeviceData(WiFiDirect.this, senderIP, port, true);

					DeviceDTO gmDevice = new DeviceDTO();
					gmDevice.setIp(senderIP);
					gmDevice.setPort(port);

					GroupControl.getInstance().setBooleanGroupOwner(true);
					GroupControl.getInstance().getIController().addDeviceDTO(gmDevice);

					tusta.s(cxt, "Yay,New Device: IP: " + senderIP + " Port: " + port);


					isWDConnected = true;
					break;
				case DataHandler.DEVICE_LIST_CHANGED:
					ArrayList<DeviceDTO> devices = DBAdapter.getInstance(WiFiDirect.this)
							.getDeviceList();
					int peerCount = (devices == null) ? 0 : devices.size();
					if (peerCount > 0) {
						progressBar.setVisibility(View.GONE);
						deviceListFragment = new PeerListFragment();
						Bundle args = new Bundle();
						args.putSerializable(PeerListFragment.ARG_DEVICE_LIST, devices);
						deviceListFragment.setArguments(args);

						FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
						ft.replace(R.id.deviceListHolder, deviceListFragment);
						ft.setTransition(FragmentTransaction.TRANSIT_NONE);
						ft.commit();
					}
					setToolBarTitle(peerCount);
					break;
				case DataHandler.CHAT_REQUEST_RECEIVED:
					DeviceDTO chatRequesterDevice = (DeviceDTO) intent.getSerializableExtra(DataHandler
							.KEY_CHAT_REQUEST);
					DialogUtils.getChatRequestDialog(WiFiDirect.this,
							chatRequesterDevice).show();
					break;
				case DataHandler.CHAT_RESPONSE_RECEIVED:
					boolean isChatRequestAccepted = intent.getBooleanExtra(DataHandler
							.KEY_IS_CHAT_REQUEST_ACCEPTED, false);
					if (!isChatRequestAccepted) {
						NotificationToast.showToast(WiFiDirect.this, "Chat request " +
								"rejected");
					} else {
						DeviceDTO chatDevice = (DeviceDTO) intent.getSerializableExtra(DataHandler
								.KEY_CHAT_REQUEST);
						DialogUtils.openChatActivity(WiFiDirect.this, chatDevice);
						/*NotificationToast.showToast(WiFiDirect.this, chatDevice
								.getPlayerName() + "Accepted Chat request");
								*/
					}
					break;
				default:
					break;

			}
		}
	};
	private DeviceDTO selectedDevice;

	public boolean getIsWifiP2pEnabled(){ return isWDConnected;}

	/**
	 * @param isWifiP2pEnabled the isWifiP2pEnabled to set
	 */
	public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
		this.isWifiP2pEnabled = isWifiP2pEnabled;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cxt = getApplicationContext();
		tusta = Tusta.getInstance();
		tusta.global = this;
		setContentView(R.layout.local_wifidirect);
		Toolbar toolbar =  findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		initialize();
	}

	public void findPeers(View v) {
		if (!isWDConnected) {
			tusta.s(getApplicationContext(),"Please connect first");
			Log.i(TAG, "findPeers: Device tried to broadcast");
		}else{
			Intent intent = new Intent(WiFiDirect.this, CastActivity.class);
			startActivity(intent);
		}
	}


	private void initialize() {
		if (Build.MANUFACTURER.equals("acer") || Build.MANUFACTURER.equals("oppo")){
			timeout = 5000;
		}
		progressBar = findViewById(R.id.progressBarLocalDash);

		String myIP = Utility.getWiFiIPAddress(WiFiDirect.this);
		Utility.saveString(WiFiDirect.this, TransferConstants.KEY_MY_IP, myIP);

		setToolBarTitle(0);

		wifiP2pManager = (WifiP2pManager) getSystemService(WIFI_P2P_SERVICE);
		wifip2pChannel = wifiP2pManager.initialize(this, getMainLooper(), null);

		// Starting Device listener with default port for now
		appController = (AppController) getApplicationContext();
		appController.startDeviceListener();

		checkWritePermission();
		wifiP2pManager.discoverPeers(wifip2pChannel, new WifiP2pManager.ActionListener() {
			@Override
			public void onSuccess() {
				Log.i(TAG, "onSuccess: Discovery started " + Build.DEVICE);
			}

			@Override
			public void onFailure(int reason) {
				Log.i(TAG, "onFailure: Device can not start Discover Peers "+ Build.DEVICE);
			}
		});
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	protected void reDiscoverPeers(){
		wifiP2pManager.requestPeers(wifip2pChannel, WiFiDirect.this);
		if(!isDestroyed() && !isFinishing()) {
			new Handler(getMainLooper()).postDelayed(this::reDiscoverPeers, 30000);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	private void startRegistrationAndDiscovery(){
		Map<String, String> record = new HashMap<String, String>();
		record.put(deviceDTO.getSsid(),deviceDTO.getPassphrase());
		GroupControl.GroupGatewayController.getInstance().setGroupRecord(record);
		WifiP2pDnsSdServiceInfo service = WifiP2pDnsSdServiceInfo.newInstance(
				SERVICE_INSTANCE, SERVICE_TYPE, record);
		wifiP2pManager.addLocalService(wifip2pChannel, service, new WifiP2pManager.ActionListener() {
			@Override
			public void onSuccess() {
				Log.d("Success Registration","Success");
				Log.d("RECORDS","SSID" + record.get(deviceDTO.getSsid()) + "PASS" + record.get(deviceDTO.getSsid()));
			}
			@Override
			public void onFailure(int i) {
				Log.d(".WiFiDirect sRAD","Message" + String.valueOf(i));
			}
		});
		discoverService();
	}

	private void discoverService(){
		final HashMap<String,String> clients=new HashMap<>();
		WifiP2pManager.DnsSdTxtRecordListener txtRecordListener = (domain, record, wifiP2pDevice) -> {
			tusta.s(WiFiDirect.this, "D:" + domain.toString());
			tusta.s(WiFiDirect.this, wifiP2pDevice.deviceName);
			if(record == null)
				Log.i("ds Record", "RECORD IS NULL", new Exception() );
			for( String key : record.keySet() ){
				Log.i("dS Record","KEY: "+ key + " , Val: "+record.get(key));
			}
		};
		WifiP2pManager.DnsSdServiceResponseListener serviceResponseListener = (instanceName, registrationType, wifiP2pDevice) -> {
			Log.d("Service Status", instanceName + "####" + registrationType);
		};
		wifiP2pManager.setDnsSdResponseListeners(wifip2pChannel,serviceResponseListener,txtRecordListener);
		WifiP2pDnsSdServiceRequest serviceRequest= WifiP2pDnsSdServiceRequest.newInstance();
		wifiP2pManager.addServiceRequest(wifip2pChannel, serviceRequest, new WifiP2pManager.ActionListener() {
			@Override
			public void onSuccess() {
				Log.d("Service Status","Added service discovery");
			}
			@Override
			public void onFailure(int i) {
				Log.d("Service Status","Cannot Add service discovery" + i);
			}
		});
		wifiP2pManager.discoverServices(wifip2pChannel, new WifiP2pManager.ActionListener() {
			@Override
			public void onSuccess() {
				Log.d("Service Status", "Service discovery initiated");
			}

			@Override
			public void onFailure(int i) {
				Log.d("Service Status", "Service discovery failed: " + i);
			}
		});
	}


	@Override
	protected void onPause() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(localDashReceiver);
		unregisterReceiver(wiFiDirectBroadcastReceiver);
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();

		IntentFilter localFilter = new IntentFilter();
		localFilter.addAction(DataHandler.DEVICE_LIST_CHANGED);
		localFilter.addAction(FIRST_DEVICE_CONNECTED);
		localFilter.addAction(DataHandler.CHAT_REQUEST_RECEIVED);
		localFilter.addAction(DataHandler.CHAT_RESPONSE_RECEIVED);
		LocalBroadcastManager.getInstance(WiFiDirect.this).registerReceiver(localDashReceiver,
				localFilter);

		IntentFilter wifip2pFilter = new IntentFilter();
		wifip2pFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
		wifip2pFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
		wifip2pFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
		wifip2pFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

		wiFiDirectBroadcastReceiver = new WiFiDirectBroadcastReceiver(wifiP2pManager,wifip2pChannel, WiFiDirect.this);
		registerReceiver(wiFiDirectBroadcastReceiver, wifip2pFilter);

		LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(DataHandler.DEVICE_LIST_CHANGED));
	}

	@Override
	protected void onDestroy() {
		appController.stopConnectionListener(); // Listener for Chat
		appController.startDeviceListener(); // Listener for new Devices
		Utility.clearPreferences(WiFiDirect.this);
		Utility.deletePersistentGroups(wifiP2pManager, wifip2pChannel);
		DBAdapter.getInstance(WiFiDirect.this).clearDatabase();
		wifiP2pManager.removeGroup(wifip2pChannel, new WifiP2pManager.ActionListener() {
			@Override
			public void onSuccess() {}
			@Override
			public void onFailure(int i) {	Log.i(TAG, "onFailure: Failed to Remove Group"); }
		});
		super.onDestroy();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
			finish();
		}
	}

	private void checkWritePermission() {
		boolean isGranted = Utility.checkPermission(WRITE_PERMISSION, this);
		if (!isGranted) {
			Utility.requestPermission(WRITE_PERMISSION, WRITE_PERM_REQ_CODE, this);
		}
	}

	@Override
	public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {
		// Takes care, what will happen after the device is connected to each other.
		if(wifiP2pInfo.groupFormed){
			if (!wifiP2pInfo.isGroupOwner && !isConnectionInfoSent) {
				appController.startConnectionListener(ConnectionUtils.getPort(WiFiDirect.this));
				String groupOwnerAddress = wifiP2pInfo.groupOwnerAddress.getHostAddress();
				DataSender.sendCurrentDeviceDataWD(WiFiDirect.this, groupOwnerAddress, TransferConstants
						.INITIAL_DEFAULT_PORT, true);
				isConnectionInfoSent = true;

				tusta.s(cxt, "Group Member");
				Log.i(TAG, "onConnectionInfoAvailable: GroupMember");

				GroupControl.getInstance().setMyPort(ConnectionUtils.getPort(WiFiDirect.this));
				GroupControl.getInstance().setBooleanGroupOwner(false);
				GroupControl.GroupMemberController.getInstance().setGoIP( groupOwnerAddress );// This is most likely 192.168.49.1 LOL

				isWDConnected = true;

				discoverService();
				Log.d(TAG+" onConnection", String.valueOf(GroupControl.GroupGatewayController.getInstance().getGroupSSIDPass()));
			}else{
				isWDConnected = true;
				GroupControl.getInstance().setMyPort(TransferConstants.GROUP_CONTROL_PORT);
				GroupControl.getInstance().setBooleanGroupOwner(true);
				tusta.s(cxt, "Group Owner");
				startRegistrationAndDiscovery();
			}
		}
	}

	@Override
	public void onPeersAvailable(WifiP2pDeviceList peers) {
		WifiP2pDevice groupOwner = null;

		ArrayList<DeviceDTO> deviceDTOs = new ArrayList<>();
		List<WifiP2pDevice> devices = (new ArrayList<>());
		devices.addAll(peers.getDeviceList());
		for (WifiP2pDevice device : devices) {
			DeviceDTO deviceDTO = new DeviceDTO();
			deviceDTO.setIp(device.deviceAddress);
			deviceDTO.setDeviceName("");
			deviceDTO.setOsVersion("");
			deviceDTO.setPort(-1);

			if (device.isGroupOwner()) {
				isGroupOwnerPresent = true;
				deviceDTO.setPlayerName("GO: " + device.deviceName);
				groupOwner = device;
			} else
				deviceDTO.setPlayerName("GM: " + device.deviceName);

				deviceDTOs.add(deviceDTO);
		}

		if (!isWDConnected && groupOwner != null) {
			discoverService();
			WifiP2pConfig config = new WifiP2pConfig();
			config.deviceAddress = groupOwner.deviceAddress;
			config.groupOwnerIntent = 1;

			wifiP2pManager.connect(wifip2pChannel, config, new WifiP2pManager.ActionListener() {
				@Override
				public void onSuccess() {
					isWDConnected = Boolean.TRUE;
				}

				@Override
				public void onFailure(int reason) {

				}
			});
		}
		progressBar.setVisibility(View.GONE);
		deviceListFragment = new PeerListFragment();
		Bundle args = new Bundle();
		args.putSerializable(PeerListFragment.ARG_DEVICE_LIST, deviceDTOs);
		deviceListFragment.setArguments(args);

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(R.id.deviceListHolder, deviceListFragment);
		ft.setTransition(FragmentTransaction.TRANSIT_NONE);
		ft.commitAllowingStateLoss();
		//ft.commit();

		// Dominate 1/28/2018

		if(!isHandlerPosted && !isWDConnected) {
			new Handler(getMainLooper())
					.post(() -> {
						if (!isGroupOwnerPresent && !isWDConnected) {
							new Handler(getMainLooper()).postDelayed(() -> {
								if (!isWDConnected && !isGroupOwnerPresent)
									wifiP2pManager.createGroup(wifip2pChannel, new WifiP2pManager.ActionListener() {
										@Override
										public void onSuccess() {
											Log.i(".WifiDirect",Build.BRAND + " " + Build.DEVICE + " " + "createGroup");
											isWDConnected = true;
										}
										@Override
										public void onFailure(int reason) {
											tusta.s(cxt, "Your Device cannot create a WifiP2P group, please restart" );
											Log.i(TAG, "onFailure: "+ String.valueOf(reason));
										}
									});
							}, 1000);
						}
					});
		}
	}

	public void onPeerDisconnect(){
		Log.v("onPeerDisconnect","~");
		isWDConnected = (false);
		//setToolBarTitle(0);
		wifiP2pManager.requestPeers(wifip2pChannel, WiFiDirect.this);
	}

	@Override
	public void onListFragmentInteraction(DeviceDTO deviceDTO) {
		if (!isWDConnected) {
			WifiP2pConfig config = new WifiP2pConfig();
			config.deviceAddress = deviceDTO.getIp();
			config.wps.setup = WpsInfo.PBC;
			config.groupOwnerIntent = 4;
			wifiP2pManager.connect(wifip2pChannel, config, new WifiP2pManager.ActionListener() {
				@Override
				public void onSuccess() {
					// Connection request succeeded. No code needed here
					GroupControl.GroupMemberController.getInstance().setSelfIP(Utility.getMyIpAddress());
				}
				@Override
				public void onFailure(int reasonCode) {
					tusta.s(cxt, "Failure: " + String.valueOf(reasonCode) );
				}
			});
		} else {
			selectedDevice = deviceDTO;
			DialogUtils.openChatActivity(WiFiDirect.this , selectedDevice);
		}
	}

	@Override
	public void onGroupInfoAvailable(WifiP2pGroup wifiP2pGroup) {
		if(wifiP2pGroup == null){
			Log.d("TAG","Group is  null");
		}else {
				String ssid = wifiP2pGroup.getNetworkName();
				String passphrase = wifiP2pGroup.getPassphrase();
				deviceDTO.setSsid(ssid);
				deviceDTO.setPassphrase(passphrase);
		}
	}

	private void setToolBarTitle(int peerCount) {
		if (getSupportActionBar() != null) {
			String title = String.format(getString(R.string.wd_title_with_count), String
					.valueOf(peerCount));
			getSupportActionBar().setTitle(title);
		}
	}
}
