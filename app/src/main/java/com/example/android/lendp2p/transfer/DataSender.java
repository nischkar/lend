package com.example.android.lendp2p.transfer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.lendp2p.GroupControl;
import com.example.android.lendp2p.Tusta;
import com.example.android.lendp2p.model.ChatDTO;
import com.example.android.lendp2p.model.DeviceDTO;
import com.example.android.lendp2p.utils.ConnectionUtils;
import com.example.android.lendp2p.utils.Utility;

public class DataSender {

	public static void sendData(Context context, String destIP, int destPort, ITransferable data) {
		Intent serviceIntent = new Intent(context,
				DataTransferService.class);
		serviceIntent.setAction(DataTransferService.ACTION_SEND_DATA);
		serviceIntent.putExtra(
				DataTransferService.DEST_IP_ADDRESS, destIP);
		serviceIntent.putExtra(
				DataTransferService.DEST_PORT_NUMBER, destPort);

		serviceIntent.putExtra(DataTransferService.EXTRAS_SHARE_DATA, data);
		context.startService(serviceIntent);
	}

	public static void sendChatNotification(Context context, String destIP, int destPort){
		Intent serviceIntent = new Intent(context,
				DataTransferService.class);
		serviceIntent.setAction(DataTransferService.ACTION_SEND_NOTIFICATION);
		serviceIntent.putExtra(DataTransferService.DEST_IP_ADDRESS, destIP);
		serviceIntent.putExtra(DataTransferService.DEST_PORT_NUMBER, destPort);
		context.startService(serviceIntent);
	}


	public static void sendFile(Context context, String destIP, int destPort, Uri fileUri) {
		Intent serviceIntent = new Intent(context,
				DataTransferService.class);
		serviceIntent.setAction(DataTransferService.ACTION_SEND_FILE);
		serviceIntent.putExtra(
				DataTransferService.DEST_IP_ADDRESS, destIP);
		serviceIntent.putExtra(
				DataTransferService.DEST_PORT_NUMBER, destPort);
		serviceIntent.putExtra(
				DataTransferService.EXTRAS_FILE_PATH, fileUri.toString());

		context.startService(serviceIntent);
	}


	public static void sendCurrentDeviceData(Context context, String destIP, int destPort,
											 boolean isRequest) {
		DeviceDTO currentDevice = new DeviceDTO();
		currentDevice.setPort(ConnectionUtils.getPort(context));
		String playerName = Utility.getString(context, TransferConstants.KEY_USER_NAME);
		if (playerName != null) {
			currentDevice.setPlayerName(playerName);
		}
		currentDevice.setIp(Utility.getMyIpAddress());
		ITransferable transferData = null;
		if (!isRequest) {
			transferData = TransferModelGenerator.generateDeviceTransferModelResponse
					(currentDevice);
		} else {
			transferData = TransferModelGenerator.generateDeviceTransferModelRequest
					(currentDevice);
		}

		sendData(context, destIP, destPort, transferData);
	}

	public static void sendCurrentDeviceSSID(Context context, String ssid, String passphrase, boolean isRequest){
		DeviceDTO currentDevice=new DeviceDTO();
		currentDevice.getSsid();
		currentDevice.getPassphrase();
		ITransferable transferData=null;
		if(!isRequest){
			transferData=TransferModelGenerator.generateDeviceTransferModelResponse(currentDevice);
		}else{
			transferData=TransferModelGenerator.generateDeviceTransferModelRequest(currentDevice);
		}
	}

	public static void sendCurrentDeviceDataWD(Context context, String destIP, int destPort,
											   boolean isRequest) {
		DeviceDTO currentDevice = new DeviceDTO();
		currentDevice.setPort(ConnectionUtils.getPort(context));
		String playerName = Utility.getString(context, TransferConstants.KEY_USER_NAME);
		if (playerName != null) {
			currentDevice.setPlayerName(playerName);
		}
		currentDevice.setIp(Utility.getMyIpAddress());
		ITransferable transferData = null;
		if (!isRequest) {
			transferData = TransferModelGenerator.generateDeviceTransferModelResponseWD
					(currentDevice);
		} else {
			transferData = TransferModelGenerator.generateDeviceTransferModelRequestWD
					(currentDevice);
		}

		sendData(context, destIP, destPort, transferData);
	}

	public static void sendChatRequest(Context context, String destIP, int destPort) {
		DeviceDTO currentDevice = new DeviceDTO();
		currentDevice.setPort(ConnectionUtils.getPort(context));
		currentDevice.setDeviceName(Build.MANUFACTURER);
		String playerName = Utility.getString(context, TransferConstants.KEY_USER_NAME);
		if (playerName != null) {
			currentDevice.setPlayerName(playerName);
		}
		currentDevice.setIp(Utility.getMyIpAddress());
		ITransferable transferData = TransferModelGenerator.generateChatRequestModel(currentDevice);
		sendData(context, destIP, destPort, transferData);
	}

	public static void sendChatResponse(Context context, String destIP, int destPort, boolean
			isAccepted) {
		DeviceDTO currentDevice = new DeviceDTO();
		currentDevice.setPort(ConnectionUtils.getPort(context));
		String playerName = Utility.getString(context, TransferConstants.KEY_USER_NAME);
		if (playerName != null) {
			currentDevice.setPlayerName(playerName);
		}
		currentDevice.setIp(Utility.getMyIpAddress());
		ITransferable transferData = TransferModelGenerator.generateChatResponseModel
				(currentDevice, isAccepted);
		sendData(context, destIP, destPort, transferData);
	}

	public static void sendChatInfo(Context context, String destIP, int destPort, ChatDTO chat) {
		ITransferable transferableData = TransferModelGenerator.generateChatTransferModel(chat);
		sendData(context, destIP, destPort, transferableData);
	}

	public static void sendBroadcastInfo(Context context,ChatDTO chat) {
		ITransferable transferableData = TransferModelGenerator.generateChatTransferModel(chat);
		if(GroupControl.getInstance().isGroupOwner()) {
			for (DeviceDTO deviceDTO : GroupControl.GroupOwnerController.getInstance().getDeviceDTOSet()) {
				String destIP = deviceDTO.getIp();
				int destPort = deviceDTO.getPort();
				if(! (destIP.equals(chat.getFromIP())) )
					sendData(context, destIP, destPort, transferableData);
			}
		}else{
		    GroupControl.GroupMemberController instance = GroupControl.GroupMemberController.getInstance();
			sendData(context,instance.getGoIP(),instance.getGoPort(), transferableData);
		}
	}
}
