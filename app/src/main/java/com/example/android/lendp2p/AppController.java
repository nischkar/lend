package com.example.android.lendp2p;

import android.app.Application;

import com.example.android.lendp2p.transfer.ConnectionListener;
import com.example.android.lendp2p.transfer.DeviceListener;
import com.example.android.lendp2p.transfer.TransferConstants;
import com.example.android.lendp2p.utils.ConnectionUtils;

public class AppController extends Application {

    private ConnectionListener connListener;
    private DeviceListener deviceListener;
    private int myPort;

    private boolean isConnectionListenerRunning = false;
	private boolean isDeviceListenerRunning = false;

    @Override
    public void onCreate() {
        super.onCreate();
        myPort = ConnectionUtils.getPort(getApplicationContext());
        connListener = new ConnectionListener(getApplicationContext(), myPort);
    }

    // FOR DEVICES THAT WANT TO CONNECT
    public void startDeviceListener(){
    	deviceListener = new DeviceListener(getApplicationContext(), TransferConstants.INITIAL_DEFAULT_PORT);
    	deviceListener.start();
    	isDeviceListenerRunning = true;
    }
    public void stopDeviceListener(){
	    if (!isDeviceListenerRunning) {
		    return;
	    }
	    if (deviceListener != null) {
		    deviceListener.tearDown();
		    deviceListener = null;
	    }
	    isDeviceListenerRunning = false;
    }

    // FOR CONNECTIONS FOR CHAT AND SUCH
    public void stopConnectionListener() {
        if (!isConnectionListenerRunning) {
            return;
        }
        if (connListener != null) {
            connListener.tearDown();
            connListener = null;
        }
        isConnectionListenerRunning = false;
    }

    public void startConnectionListener() {
        if (isConnectionListenerRunning) {
            return;
        }
        if (connListener == null) {
            connListener = new ConnectionListener(getApplicationContext(), myPort);
        }
        if (!connListener.isAlive()) {
            connListener.interrupt();
            connListener.tearDown();
            connListener = null;
        }
        connListener = new ConnectionListener(getApplicationContext(), myPort);
        connListener.start();
        isConnectionListenerRunning = true;
    }

    public void startConnectionListener(int port) {
        myPort = port;
        startConnectionListener();
    }

    public void restartConnectionListenerWith(int port) {
        stopConnectionListener();
        startConnectionListener(port);
    }

    public boolean isConnListenerRunning() {
        return isConnectionListenerRunning;
    }

    public boolean isDeviceListenerRunning(){ return isDeviceListenerRunning; }

    public int getPort(){
        return myPort;
    }
}
