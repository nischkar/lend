package com.example.android.lendp2p.transfer;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.android.lendp2p.GroupControl;
import com.example.android.lendp2p.Tusta;
import com.example.android.lendp2p.WiFiDirect;
import com.example.android.lendp2p.db.DBAdapter;
import com.example.android.lendp2p.model.DeviceDTO;

import java.util.TreeSet;

public class DeviceDataHandler {

	public static final String DEVICE_LIST_CHANGED = "device_list_updated";

	public static final String CHAT_REQUEST_RECEIVED = "chat_request_received";
	public static final String CHAT_RESPONSE_RECEIVED = "chat_response_received";
	public static final String KEY_CHAT_REQUEST = "chat_requester_or_responder";
	public static final String KEY_IS_CHAT_REQUEST_ACCEPTED = "is_chat_request_Accepter";


	private ITransferable data;
	private Context mContext;
	private String senderIP;
	private LocalBroadcastManager broadcaster;
	private DBAdapter dbAdapter = null;

	DeviceDataHandler(Context context, String senderIP, ITransferable data) {
		this.mContext = context;
		this.data = data;
		this.senderIP = senderIP;
		this.dbAdapter = DBAdapter.getInstance(mContext);
		this.broadcaster = LocalBroadcastManager.getInstance(mContext);
	}

	public void process() {
		if (data.getRequestType().equalsIgnoreCase(TransferConstants.TYPE_REQUEST)) {
			processRequest();
		} else {
			processResponse();
		}
	}

	private void processRequest() {
		switch (data.getRequestCode()) {
			case TransferConstants.CLIENT_DATA:
				processPeerDeviceInfo();
				DataSender.sendCurrentDeviceData(mContext, senderIP,
						dbAdapter.getDevice(senderIP).getPort(), false);
				break;
			case TransferConstants.CLIENT_DATA_WD:
				processPeerDeviceInfo();
				Intent intent = new Intent(WiFiDirect.FIRST_DEVICE_CONNECTED);
				intent.putExtra(WiFiDirect.KEY_FIRST_DEVICE_IP, senderIP);
				broadcaster.sendBroadcast(intent);
				break;
			default:
				break;
		}
	}

	private void processResponse() {
		switch (data.getRequestCode()) {
			case TransferConstants.CLIENT_DATA:
			case TransferConstants.CLIENT_DATA_WD:
				processPeerDeviceInfo();
				break;
			case TransferConstants.GROUP_CONTROL_UPDATE_DEVICE:
				processUpdateDevice();
			default:
				break;
		}
	}

	private void processPeerDeviceInfo() {
		String deviceJSON = data.getData();
		DeviceDTO device = DeviceDTO.fromJSON(deviceJSON);
		device.setIp(senderIP);
		long rowid = dbAdapter.addDevice(device);

		if (!GroupControl.getInstance().isGroupOwner()) {
			GroupControl.GroupMemberController.getInstance().setGoIP(senderIP);
			GroupControl.GroupMemberController.getInstance().setGoPort(device.getPort());
		}
		if (rowid > 0) {
			Log.d("DXDX", Build.MANUFACTURER + " received: " + deviceJSON);
		} else {
			Log.e("DXDX", Build.MANUFACTURER + " can't save: " + deviceJSON);
		}

		Intent intent = new Intent(DEVICE_LIST_CHANGED);
		broadcaster.sendBroadcast(intent);
	}

	private void processUpdateDevice() {
		Tusta.getInstance().s_outside(Tusta.getInstance().global.getApplicationContext(), "Not yet Implemented: proccessUpdate");
		TreeSet<DeviceDTO> deviceDTOS = (TreeSet<DeviceDTO>) ((GroupControl.DeviceCollection) data).getDeviceDTOSet(); // SRSLY?
		//GroupControl.getInstance().getIController()
	}
}
