/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.android.lendp2p.algo.closepair;

/**
 *
 * @author SOFIA
 */
class Coordinate implements Comparable<Coordinate> {
 private double x;
 private double y;  
 
 public Coordinate() {
  this(0, 0);
 }
 public Coordinate(double x, double y) {
  super();
  this.x = x;
  this.y = y;
 }
 
 public double getX() {
  return x;
 }
 public void setX(double x) {
  this.x = x;
 }
 public double getY() {
  return y;
 }
 public void setY(double y) {
  this.y = y;
 }
 @Override
 public int compareTo(Coordinate point) {
  if (x > point.x) {
   return 1;
  } else if (x < point.x) {
   return -1;
  } else {
   if (y > point.y) {
    return 1;
   } else if (y < point.y) {
    return -1;
   } else {
    return 0;
   }
  }
 }  
 @Override
 public String toString() {
  return "x: " + x + ",\ty: " + y;
 }
 
}
