package com.example.android.lendp2p;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.android.lendp2p.model.ChatDTO;
import com.example.android.lendp2p.notification.NotificationToast;
import com.example.android.lendp2p.transfer.DataSender;
import com.example.android.lendp2p.utils.ConnectionUtils;
import com.example.android.lendp2p.utils.Utility;

import java.util.ArrayList;
import java.util.List;

public class CastActivity  extends AppCompatActivity {

	public static final String ACTION_BROADCAST_RECEIVED = "com.example.android.lendp2p.broadcastreceived";
	public static final String KEY_CHAT_DATA = "chat_data_key";

	EditText etChat;
	RecyclerView chatListHolder;
	private List<ChatDTO> chatList;
	private ChatListAdapter chatListAdapter;

	private String selfMac;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);

		initialize();

		chatListHolder = (RecyclerView) findViewById(R.id.chat_list);
		etChat = (EditText) findViewById(R.id.et_chat_box);

		chatList = new ArrayList<>();
		chatListAdapter = new ChatListAdapter(chatList);
		chatListHolder.setAdapter(chatListAdapter);

		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CastActivity.this);
		linearLayoutManager.setStackFromEnd(true);
		chatListHolder.setLayoutManager(linearLayoutManager);
	}

	private void initialize() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(ACTION_BROADCAST_RECEIVED);
		LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(castReceiver, filter);

		Bundle extras = getIntent().getExtras();

		setToolBarTitle("Chat with " + "Group Chat");
	}

	public void SendChatInfo(View v) {
		String message = etChat.getText().toString();

		ChatDTO myChat = new ChatDTO();
		myChat.setPort(ConnectionUtils.getPort(CastActivity.this));
		myChat.setFromIP(Utility.getString(CastActivity.this, "myip"));
		myChat.setLocalTimestamp(System.currentTimeMillis());
		myChat.setMessage(message);
		myChat.setSentBy("Group Chat");
		myChat.setMyChat(true);
		DataSender.sendBroadcastInfo(CastActivity.this, myChat);
		etChat.setText("");
		updateChatView(myChat);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private BroadcastReceiver castReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			switch (intent.getAction()) {
				case ACTION_BROADCAST_RECEIVED:
					ChatDTO chat = (ChatDTO) intent.getSerializableExtra(KEY_CHAT_DATA);
					chat.setMyChat(false);
					updateChatView(chat);
					if(GroupControl.getInstance().isGroupOwner()){
						DataSender.sendBroadcastInfo(CastActivity.this, chat);
					}
					break;
				default:
					break;
			}
		}
	};

	private void updateChatView(ChatDTO chatObj) {
		chatList.add(chatObj);
		chatListAdapter.notifyDataSetChanged();
		chatListHolder.smoothScrollToPosition(chatList.size() - 1);
	}

	private class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ChatHolder> {

		private List<ChatDTO> chatList;

		ChatListAdapter(List<ChatDTO> chatList) {
			this.chatList = chatList;
		}

		@Override
		public ChatHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			if (viewType == 0) {
				View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_mine,
						parent, false);
				return new ChatHolder(itemView);
			} else {
				View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_other,
						parent, false);
				return new ChatHolder(itemView);
			}
		}

		@Override
		public void onBindViewHolder(ChatHolder holder, int position) {
			holder.bind(chatList.get(position));
		}

		@Override
		public int getItemCount() {
			return chatList == null ? 0 : chatList.size();
		}

		@Override
		public int getItemViewType(int position) {
			ChatDTO chatObj = chatList.get(position);
			return (chatObj.isMyChat() ? 0 : 1);
		}

		class ChatHolder extends RecyclerView.ViewHolder {
			TextView tvChatMessage;

			public ChatHolder(View itemView) {
				super(itemView);
				tvChatMessage = (TextView) itemView.findViewById(R.id.tv_chat_msg);
			}

			public void bind(ChatDTO singleChat) {
				tvChatMessage.setText(singleChat.getMessage());
			}
		}
	}

	private void setToolBarTitle(String title) {
		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(title);
		}
	}
}
