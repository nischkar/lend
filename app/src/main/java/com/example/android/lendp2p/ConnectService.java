package com.example.android.lendp2p;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.os.IBinder;
import android.support.annotation.Nullable;

import com.example.android.lendp2p.model.DeviceDTO;
import com.example.android.lendp2p.utils.ConnectionUtils;

import android.util.Log;
import android.widget.Toast;
import com.example.android.lendp2p.transfer.DataSender;

import com.example.android.lendp2p.model.ChatDTO;
import com.example.android.lendp2p.utils.Utility;

/**
 * Created by Vlad Villanueva on 16/12/2017.
 * PACKAGE_NAME for LENDP2P
 */

public class ConnectService extends Service {
	Boolean isInit = false;

	private GroupControl.IController controller;
	private Tusta tusta;
	private Context cxt;

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if(!isInit){
			init();
			isInit = Boolean.TRUE;
		}
		ChatDTO chatDTO = (ChatDTO) intent.getSerializableExtra(ChatActivity.KEY_CHAT_DATA);
		HopMessage(chatDTO,chatDTO.getReceiverID());
		return super.onStartCommand(intent, flags, startId);
	}

	public void init(){
		Toast.makeText(this, "Service Init" , Toast.LENGTH_SHORT).show();
		tusta = Tusta.getInstance();
		cxt = getApplicationContext();
		controller = GroupControl.getInstance().getIController();
	}

	/**
	 *
	 * @param myChat CHAT OBJECT
	 * @param source WHERE THE CHAT CAME FROM, MAY NOT BE THE SENDER
	 *
	 */

	private void HopMessage(ChatDTO myChat,String source){
		// Make it hop to Group Owner
		Tusta.getInstance().s(getApplicationContext(), "Received: "+myChat.toString());
		myChat.setPort(ConnectionUtils.getPort(ConnectService.this));
		myChat.setFromIP(source);
		myChat.setLocalTimestamp(System.currentTimeMillis());

		tusta.s(cxt, "Number of devices: "+ controller.getDeviceDTOSet().size() );
		for(DeviceDTO device : controller.getDeviceDTOSet()){
			DataSender.sendChatInfo(ConnectService.this, device.getIp(),
					device.getPort(), myChat);
		}
	}

	public static class HopReceiver extends BroadcastReceiver{
		public HopReceiver(){
			Log.v("HOPRECEIVER","DEFAULT CONSTRUCTOR");
		}
		public HopReceiver(Context cxt){
			Tusta.getInstance().s(cxt,"HopReceiver Running @ background");
		}
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			// Only Multi-hop when the address of the device is not the same as the one on the ChatDTO
			switch (action){
				case ChatActivity.ACTION_CHAT_RECEIVED :
					if ( !( ((ChatDTO) intent.getSerializableExtra(ChatActivity.KEY_CHAT_DATA))
							.getReceiverID().equals(Utility.getMyIpAddress()) ) ){
						Intent flood = new Intent(context,ConnectService.class);
						// Put the ChatDTO not the intent, #BUGS
						flood.putExtra(ChatActivity.KEY_CHAT_DATA,intent.getSerializableExtra(ChatActivity.KEY_CHAT_DATA));
						context.getApplicationContext().startService(flood);
					}
				break;
			}


		}
	}

}
