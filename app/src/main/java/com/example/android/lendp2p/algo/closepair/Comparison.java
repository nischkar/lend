/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.android.lendp2p.algo.closepair;

import java.util.Comparator;

/**
 *
 * @author SOFIA
 */
class Comparison implements Comparator<Coordinate> {
 
 @Override
 public int compare(Coordinate point1, Coordinate point2) {
  if (point1.getY() > point2.getY()) {
   return 1;
  } else if (point1.getY() < point2.getY()) {
   return -1;
  } else {
   if (point1.getX() > point2.getX()) {
    return 1;
   } else if (point1.getX() < point2.getX()) {
    return -1;
   } else {
    return 0;
   }
  }
 } 

}