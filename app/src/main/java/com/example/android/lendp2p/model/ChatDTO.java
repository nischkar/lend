package com.example.android.lendp2p.model;

import android.widget.Toast;

import com.example.android.lendp2p.Tusta;
import com.google.gson.Gson;

import java.io.Serializable;

public class ChatDTO implements Serializable {

	private String message;
	private String sentBy;
	private long localTimestamp;
	private String fromIP;
	private int port;

	private String receiverID = "0.0.0.0";
	private boolean isMyChat = false;

	// ~
	public void setReceiverID(String address) { receiverID = address;}

	public String getReceiverID(){
	    if(receiverID != null)
	    	return receiverID;
	    else
            Tusta.getInstance().s(Tusta.getInstance().global.getApplicationContext(),"No ID");
	    return "0.0.0.0";
	}
	// ~

	public boolean isMyChat() {
		return isMyChat;
	}

	public void setMyChat(boolean myChat) {
		isMyChat = myChat;
	}

	public String getMessage() {
		return message;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSentBy() {
		return sentBy;
	}

	public void setSentBy(String sentBy) {
		this.sentBy = sentBy;
	}

	public long getLocalTimestamp() {
		return localTimestamp;
	}

	public void setLocalTimestamp(long localTimestamp) {
		this.localTimestamp = localTimestamp;
	}

	public String getFromIP() {
		return fromIP;
	}

	public void setFromIP(String fromIP) {
		this.fromIP = fromIP;
	}

	@Override
	public String toString() {
		String stringRep = (new Gson()).toJson(this);
		return stringRep;
	}

	public static ChatDTO fromJSON(String jsonRep) {
		Gson gson = new Gson();
		ChatDTO chatObject = gson.fromJson(jsonRep, ChatDTO.class);
		return chatObject;
	}
}
