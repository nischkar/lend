package com.example.android.lendp2p;

import android.util.Log;

import com.example.android.lendp2p.model.DeviceDTO;
import com.example.android.lendp2p.transfer.ITransferable;
import com.example.android.lendp2p.transfer.TransferConstants;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Vlad Villanueva on 28/12/2017.
 * com.example.android.lendp2p for LENDP2P
 */

public class GroupControl {
	private static GroupControl instance;
	private static GroupOwnerController goController;
	private static GroupMemberController gmController;
	private static GroupGatewayController gwController;

	private GroupControl(){	}

	public static GroupControl getInstance() {
		if(instance == null){
			instance = new GroupControl();
		}
		return instance;
	}

	private boolean isGO = false;
	public boolean isGroupOwner(){ return isGO; }
	public void setBooleanGroupOwner(boolean isGO){ this.isGO = isGO; }

	public int getMyPort() {
		if(myPort == -1)
			Log.e("GroupControl", "Port: -1 / Not available");
		return myPort;
	}

	public void setMyPort(int myPort) {
		this.myPort = myPort;
	}

	private int myPort = -1;

	public IController getIController(){
		if(isGO){
			return GroupOwnerController.getInstance();
		}else{
			return GroupMemberController.getInstance();
		}
	}

	public static class GroupOwnerController implements IController{
		private GroupOwnerController() { this.selfPort = GroupControl.getInstance().myPort;deviceDTOSet = new TreeSet<>(); }
		public static GroupOwnerController getInstance() {
			if(goController == null){
				goController = new GroupOwnerController();
			}
			return goController;
		}

		private Set<DeviceDTO> deviceDTOSet;
		@Override
		public Set<DeviceDTO> getDeviceDTOSet() {
			return deviceDTOSet;
		}

		@Override
		public void setDeviceDTOSet(Set<DeviceDTO> deviceDTOS) { deviceDTOSet = deviceDTOS;	}

		@Override
		public void addDeviceDTO(DeviceDTO deviceDTO) {
			deviceDTOSet.add(deviceDTO);
		}

		private String selfMac;
		public String getMac(){ return selfMac;	}
		public void setMac(String selfMac){ this.selfMac = selfMac; }

		private String selfIP;
		public void setSelfIP(String selfIP){
			this.selfIP = selfIP;
		}
		public String getSelfIP(){
			return selfIP;
		}

		private int selfPort;
		public int getSelfPort() { return selfPort; }
		public void setSelfPort(int selfPort) {	this.selfPort = selfPort; }
	}

	public static class GroupMemberController implements IController{
		private String selfMac;
		public String getMac(){ return selfMac;	}
		public void setMac(String selfMac){ this.selfMac = selfMac; }

		private String selfIP;
		public void setSelfIP(String selfIP){
			this.selfIP = selfIP;
		}
		public String getSelfIP(){
			return selfIP;
		}

		private int selfPort;
		public int getSelfPort() { return selfPort; }
		public void setSelfPort(int selfPort) {	this.selfPort = selfPort; }

		private int goPort;
		public int getGoPort() { return goPort;	}
		public void setGoPort(int goPort) {	this.goPort = goPort; }

		private String goIP;
		public String getGoIP() { return goIP; }
		public void setGoIP(String goIP) { this.goIP = goIP; }

		private GroupMemberController() { this.selfPort = GroupControl.getInstance().myPort; }
		public static GroupMemberController getInstance() {
			if(gmController == null){
				gmController = new GroupMemberController();
			}
			return gmController;
		}

		@Override
		public Set<DeviceDTO> getDeviceDTOSet() {
			Set<DeviceDTO> deviceDTOS = new TreeSet<>();
			DeviceDTO device = new DeviceDTO();                 // Pertains to this device
			device.setPort(TransferConstants.GROUP_CONTROL_PORT);
			device.setIp(GroupMemberController.this.goIP);
			deviceDTOS.add(device);
			return deviceDTOS;
		}

		@Override
		public void setDeviceDTOSet(Set<DeviceDTO> deviceDTOS) {
			//
		}

		@Override
		public void addDeviceDTO(DeviceDTO deviceDTO) {
			setGoPort(deviceDTO.getPort());
			setGoIP(deviceDTO.getIp());
		}
	}
	public static class GroupGatewayController implements IController{

		public static GroupGatewayController getInstance() {
			if(gwController == null){
				gwController = new GroupGatewayController();
			}
			return gwController;
		}

		private String goCurrentIP;
		public String getCurrentGoIP() { return goCurrentIP; }
		public void setCurrentGoIP(String goIP) { this.goCurrentIP = goCurrentIP; }

		private String goOtherIP;
		public String getOtherGoIP() { return goOtherIP;}
		public void setOtherGoIP(String goOtherIP) { this.goOtherIP = goOtherIP;}

		private String SSID;
		public String getSSID(){ return SSID;}
		public void setSSID(String SSID){this.SSID=SSID;}

		private String Passphrase;
		public String getPassphrase(){return Passphrase;}
		public void setPassphrase(String Passphrase){this.Passphrase=Passphrase;}


		@Override
		public Set<DeviceDTO> getDeviceDTOSet() { //same functionality as GM to get list of devices
			return null;
		}

		@Override
		public void setDeviceDTOSet(Set<DeviceDTO> deviceDTOS) { return;}

		@Override
		public void addDeviceDTO(DeviceDTO deviceDTO) {}


		String selfIP;
		@Override
		public String getSelfIP() { return selfIP; }

		@Override
		public void setSelfIP(String selfIP) {}

		private Set<DeviceDTO> deviceGOSet;
		public Set<DeviceDTO> getGODeviceListSet(){return deviceGOSet;} //list of device of GO w/ mac add
		public void setGODeviceListSet (Set<DeviceDTO> GODevices){ this.deviceGOSet=GODevices;}

		private Set<DeviceDTO> availableGO;
		public Set<DeviceDTO> getAvailableGO(){ return availableGO; } //list of available GO
		public void setAvailableGO(Set<DeviceDTO> availableGO) { this.availableGO=availableGO;}

		private Set<DeviceDTO> SenderGOInfo;
		public Set<DeviceDTO> getSenderGOInfo(){return SenderGOInfo; }//group info of current group
		public void setSenderGOInfo(Set<DeviceDTO> senderGOInfo){ this.SenderGOInfo=senderGOInfo;}

		private Set<DeviceDTO> ReceiverGOInfo;
		public Set<DeviceDTO> getRecevierGOInfo(){return ReceiverGOInfo; }//group info of receiver group
		public void setReceiverGOInfo(Set<DeviceDTO> receiverGOInfo){ this.ReceiverGOInfo=receiverGOInfo;}

		private Map<String,String> GroupSSIDPass;
		public Map<String,String> getGroupSSIDPass(){return GroupSSIDPass;}
		public void setGroupRecord(Map<String, String> groupSSIDPass){this.GroupSSIDPass=groupSSIDPass;}


	}

	public static class DeviceCollection implements ITransferableGroupInfo{
		Set<DeviceDTO> deviceDTOSet;
		DeviceDTO selfDevice;


		// Prevent empty constructor
		private DeviceCollection(){}

		public DeviceCollection(Set<DeviceDTO> devices, DeviceDTO self){
			this.deviceDTOSet = devices;
			this.selfDevice = self;
		}

		@Override
		public int getRequestCode() {
			return TransferConstants.GROUP_CONTROL_UPDATE_DEVICE;
		}
		@Override
		public String getRequestType() {
			return TransferConstants.TYPE_RESPONSE;
		}

		@Override
		public String getData() {
			Log.e("DeviceCollection:","Data, Non-String");
			return "Data is not String, Use getList";
		}
		@Override
		public DeviceDTO getSource() {
			return selfDevice;
		}

		@Override
		public Set<DeviceDTO> getDeviceDTOSet() {
			return deviceDTOSet;
		}
	}

	public interface ITransferableGroupInfo extends ITransferable{
		DeviceDTO getSource();
		Set<DeviceDTO> getDeviceDTOSet();

	}
	public interface IController{
		Set<DeviceDTO> getDeviceDTOSet();
		void setDeviceDTOSet(Set<DeviceDTO> deviceDTOS);
		void addDeviceDTO(DeviceDTO deviceDTO);
		String getSelfIP();
		void setSelfIP(String selfIP);
	}

}

/*
	( )
	| |--------------------------
	| |☆ \ ...................  |
	| |   \ ..................  |
	| |    \ .................  |
	| |  ☆  \__________________|
	| |     /                  |
	| |    /                   |
	| |   /                    |
	| |☆ /                     |
	| |-------------------------
	| |
	| |
	| |
	| |
	| |

	Ako ay Pilipino
	Ang dugo'y maharlika
	Likas sa aking puso
	Adhikaing kay ganda
	Sa Pilipinas na aking bayan
	Lantay na Perlas ng Silanganan
	Wari'y natipon ang kayamanan ng Maykapal

	Bigay sa 'king talino
	Sa mabuti lang laan
	Sa aki'y katutubo
	Ang maging mapagmahal

	Chorus:
	Ako ay Pilipino,
	Ako ay Pilipino
	Isang bansa isang diwa
	Ang minimithi ko
	Sa Bayan ko't Bandila
	Laan Buhay ko't Diwa
	Ako ay Pilipino,
	Pilipinong totoo
	Ako ay Pilipino,
	Ako ay Pilipino
	Taas noo kahit kanino
	Ang Pilipino ay ako!

 */