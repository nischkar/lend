package com.example.android.lendp2p;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.Toast;


/**
 * Created by Vlad Villanueva on 18/12/2017.
 * com.example.android.lendp2p for LENDP2P
 */

public class Tusta {
	// Singleton
	private  static Tusta instance;
	private Tusta() {
		// Empty Constructor
	}
	public static Tusta getInstance() {
		if(instance == null)
			instance = new Tusta();
		return instance;
	}

	// Toast Methods
	public void l(@NonNull final Context cxt, @NonNull final String message){
				Toast.makeText(cxt,message,Toast.LENGTH_LONG).show();
	}
	public void s(@NonNull final Context cxt, @NonNull final String message) {
		Toast.makeText(cxt, message, Toast.LENGTH_SHORT).show();
	}
	public void s_outside(@NonNull final Context cxt, @NonNull final String message){
		new Handler(Looper.getMainLooper())
				.post(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(cxt, message, Toast.LENGTH_SHORT).show();
					}
				});
	}
	public void l_outside(@NonNull final Context cxt, @NonNull final String message){
		new Handler(Looper.getMainLooper())
				.post(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(cxt, message, Toast.LENGTH_SHORT).show();
					}
				});
	}
	public WiFiDirect global;
}