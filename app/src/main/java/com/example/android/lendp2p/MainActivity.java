package com.example.android.lendp2p;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);

        message = (Button) findViewById(R.id.buttonMsg);

        setWifi();
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, HomeScreen.class);
                MainActivity.this.startActivity(myIntent);
            }
        });
    }

    protected void setWifi(){
        final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if(!wifiManager.isWifiEnabled())
            wifiManager.setWifiEnabled(true);
        else{
            wifiManager.setWifiEnabled(false);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
					wifiManager.setWifiEnabled(true);
                }
            },2650);
        }
    }
}
