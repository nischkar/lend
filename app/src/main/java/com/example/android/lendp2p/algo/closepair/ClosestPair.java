package com.example.android.lendp2p.algo.closepair;

import java.util.ArrayList;
import java.util.Arrays;

/**
* @author SOFIA
*/
class ClosestPair {
	private Coordinate p1;
	private Coordinate p2;


	public static ClosestPair getClosestPair(Coordinate[] points) {
		Arrays.sort(points);
		Coordinate[] pointsOrderedOnY = points.clone();
		Arrays.sort(pointsOrderedOnY, new Comparison());
		return distance(points, 0, points.length - 1, pointsOrderedOnY);
	}

	public static ClosestPair getClosestPair(double[][] points) {
		Coordinate[] p = new Coordinate[points.length];
		for (int i = 0; i < points.length; i++) {
			p[i] = new Coordinate(points[i][0], points[i][1]);
		}
		return getClosestPair(p);
	}

	public Coordinate getP1() {
		return p1;
	}

	public void setP1(Coordinate p1) {
		this.p1 = p1;
	}

	public Coordinate getP2() {
		return p2;
	}

	public void setP2(Coordinate p2) {
		this.p2 = p2;
	}

	public ClosestPair(Coordinate p1, Coordinate p2) {
		super();
		this.p1 = p1;
		this.p2 = p2;
	}

	public double getDistance() {
		return distance(p1, p2);
	}

	public static double distance(Coordinate p1, Coordinate p2) {
		return distance(p1.getX(), p1.getY(), p2.getX(), p2.getY());
	}

	public static double distance(double x1, double y1, double x2, double y2) {
		return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
	}

	public static ClosestPair distance(Coordinate[] pointX, int minimum, int maximum, Coordinate[] pointY) {

		ArrayList<Coordinate> L = new ArrayList<Coordinate>();
		ArrayList<Coordinate> R = new ArrayList<Coordinate>();

		if (minimum >= maximum) {
			return null;
		} else if (minimum + 1 == maximum) {
			return new ClosestPair(pointX[minimum], pointX[maximum]);
		}

		int halfSize = (minimum + maximum) / 2;
		ClosestPair p1 = distance(pointX, minimum, halfSize, pointY);
		ClosestPair p2 = distance(pointX, halfSize + 1, maximum, pointY);

		double distance = 0;
		ClosestPair point = null;

		if (p1 == null && p2 == null) {
			distance = Double.MAX_VALUE;
		} else if (p1 == null) {
			distance = p2.getDistance();
			point = p2;
		} else if (p2 == null) {
			distance = p1.getDistance();
			point = p1;
		} else {
			distance = Math.min(p1.getDistance(), p2.getDistance());
			point = ((p1.getDistance() <= p2.getDistance()) ? p1 : p2);
		}
		for (int i = 0; i < pointY.length; i++) {
			if ((pointY[i].getX() <= pointX[halfSize].getX()) &&
					(pointY[i].getX() >= pointX[halfSize].getX() - distance)) {
				L.add(pointY[i]);
			} else {
				R.add(pointY[i]);
			}
		}
		double totalDist = distance;
		int index = 0;
		for (int i = 0; i < L.size(); i++) {
			while (index < R.size() && L.get(i).getY() > R.get(index).getY() + distance) {
				index++;
			}

			int index1 = index;
			while (index1 < R.size() && R.get(index1).getY() <= L.get(i).getY() + distance) {
				if (totalDist > distance(L.get(i), R.get(index1))) {
					totalDist = distance(L.get(i), R.get(index1));
					point.p1 = L.get(i);
					point.p2 = R.get(index1);
				}
				index1 = index1 + 1;
			}
		}

		return point;
	}
}
