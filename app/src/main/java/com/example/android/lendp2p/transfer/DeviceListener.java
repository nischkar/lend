package com.example.android.lendp2p.transfer;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.example.android.lendp2p.utils.Utility;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.Socket;


public class DeviceListener extends Thread {

	private int mPort;
	private Context mContext;
	private ServerSocket mServer;

	private boolean acceptRequests = true;

	public DeviceListener(Context context, int port) {
		this.mContext = context;
		this.mPort = port;
	}

	@Override
	public void run() {
		try {
			Log.d("DXDXtry", Build.MANUFACTURER + ": Device listener: " + mPort);
			mServer = new ServerSocket(mPort);
			mServer.setReuseAddress(true);

			if (mServer != null && !mServer.isBound()) {
				//mServer.bind(new InetSocketAddress(mPort));
			}

			Log.d("DDDD", "Inet4Address: " + Inet4Address.getLocalHost().getHostAddress());

			Socket socket = null;
			while (acceptRequests) {
				// this is a blocking operation
				socket = mServer.accept();
				handleData(socket.getInetAddress().getHostAddress(), socket.getInputStream());
			}
			Log.e("DXDX", Build.MANUFACTURER + ": Device listener terminated. " +
					"acceptRequests: " + acceptRequests);
			socket.close();

		} catch (IOException e) {
			Log.e("DXDX", Build.MANUFACTURER + ": Device listener EXCEPTION. " + e.toString());
		}
	}

	private void handleData(String senderIP, InputStream inputStream) {
		try {
			byte[] input = Utility.getInputStreamByteArray(inputStream);

			ObjectInput oin = null;
			try {
				oin = new ObjectInputStream(new ByteArrayInputStream(input));
				ITransferable transferObject = (ITransferable) oin.readObject();

				//processing incoming data
				(new DeviceDataHandler(mContext, senderIP, transferObject)).process();

				oin.close();
				return;

			} catch (ClassNotFoundException cnfe) {
				Log.e("DDDD", cnfe.toString());
				cnfe.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} finally {
				if (oin != null) {
					oin.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void tearDown() {
		acceptRequests = false;
	}
}
