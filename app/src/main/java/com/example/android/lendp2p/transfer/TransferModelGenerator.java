package com.example.android.lendp2p.transfer;


import com.example.android.lendp2p.model.ChatDTO;
import com.example.android.lendp2p.model.DeviceDTO;

public class TransferModelGenerator {

    public static ITransferable generateDeviceTransferModelRequest(DeviceDTO device) {
	    return new TransferModel(TransferConstants.CLIENT_DATA, TransferConstants.TYPE_REQUEST,
	            device.toString());
    }

    public static ITransferable generateDeviceTransferModelResponse(DeviceDTO device) {
	    return new TransferModel(TransferConstants.CLIENT_DATA, TransferConstants.TYPE_RESPONSE,
	            device.toString());
    }

    public static ITransferable generateDeviceTransferModelRequestWD(DeviceDTO device) {
	    return new TransferModel(TransferConstants.CLIENT_DATA_WD, TransferConstants.TYPE_REQUEST,
	            device.toString());
    }

    public static ITransferable generateDeviceTransferModelResponseWD(DeviceDTO device) {
	    return new TransferModel(TransferConstants.CLIENT_DATA_WD, TransferConstants.TYPE_RESPONSE,
	            device.toString());
    }

    public static ITransferable generateChatTransferModel(ChatDTO chat) {
        //All chats are type response as no further response is needed as of now
	    return new TransferModel(TransferConstants.CHAT_DATA,
	            TransferConstants.TYPE_RESPONSE,
	            chat.toString());
    }

    public static ITransferable generateChatRequestModel(DeviceDTO device) {
	    return new TransferModel(TransferConstants.CHAT_REQUEST_SENT,
	            TransferConstants.TYPE_REQUEST, device.toString());
    }

    public static ITransferable generateChatResponseModel(DeviceDTO device, boolean
            isChatRequestAccepted) {
        int reqCode = isChatRequestAccepted ? TransferConstants.CHAT_REQUEST_ACCEPTED :
                TransferConstants.CHAT_REQUEST_REJECTED;
        return  new TransferModel(reqCode,
                TransferConstants.TYPE_RESPONSE, device.toString());
    }

    public static ITransferable generateChatNotification(DeviceDTO deviceDTO){
    	return new TransferModel(TransferConstants.CHAT_NOTIFICATION,
			    TransferConstants.TYPE_REQUEST, deviceDTO.toString());
    }
    static class TransferModel implements ITransferable {

        int reqCode;
        String reqType;
        String data;

        TransferModel(int reqCode, String reqType, String data) {
            this.reqCode = reqCode;
            this.reqType = reqType;
            this.data = data;
        }

        @Override
        public int getRequestCode() {
            return reqCode;
        }

        @Override
        public String getRequestType() {
            return reqType;
        }

        @Override
        public String getData() {
            return data;
        }
    }
}
