package com.example.android.lendp2p.model;

import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import java.io.Serializable;

public class DeviceDTO implements Serializable, Comparable<DeviceDTO> {

    private String deviceName = Build.MODEL;
    private String osVersion = Build.VERSION.RELEASE;
    private String playerName = Build.MANUFACTURER;
    private String ip;
    private int port;
    private String deviceMac;
    private String ssid;
    private String passphrase;

    public void setSsid(String ssid){
        if(ssid!=null) {
            this.ssid = ssid;
        }
        else{
            Log.d("SET IS NULL","NULL");
        }
    }

    public DeviceDTO(){}
    public DeviceDTO(String ip, int port){
        this.ip = ip;
        this.port = port;
    }
    public String getSsid(){ return ssid;}

    public void setPassphrase(String passphrase) { this.passphrase=passphrase; }

    public String getPassphrase(){ return passphrase; }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getDeviceMac(){ return deviceMac;}

    public void setDeviceMac(String deviceMac) { this.deviceMac=deviceMac;}

    @Override
    public String toString() {
        String stringRep = (new Gson()).toJson(this);
        return stringRep;
    }

    public static DeviceDTO fromJSON(String jsonRep) {
        Gson gson = new Gson();
        DeviceDTO deviceDTO = gson.fromJson(jsonRep, DeviceDTO.class);
        return deviceDTO;
    }

    @Override
    public int compareTo(@NonNull DeviceDTO o) {
        if( (DeviceDTO.this.getIp().equals( o.getIp()) ) &&
                (DeviceDTO.this.getDeviceName().equals( o.getDeviceName()) &&
                        (DeviceDTO.this.getPort() == o.getPort())) ){
			return 0;
        }
        return 1;
    }
}
